#!/bin/sh
openssl x509 -inform der -in distribution.cer -out distribution.pem
openssl pkcs12 -export -in distribution.pem -inkey distribution.key -out distribution.p12
openssl x509 -inform der -in apns.cer -out apns.pem
openssl pkcs12 -export -in apns.pem -inkey apns.key -out apns.p12
rm *.pem
rm *.csr
rm *.cer
rm *.key
