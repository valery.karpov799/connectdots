#!/bin/sh
openssl req -config csr.conf -new -newkey rsa:2048 -nodes -keyout distribution.key -out distribution.csr
openssl req -config csr.conf -new -newkey rsa:2048 -nodes -keyout apns.key -out apns.csr
