//
//  GKRandom.swift
//  ConnectDotsTests
//
//  Created by Mark on 03.04.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation

public protocol GKRandom {
    func nextInt() -> Int

    func nextInt(upperBound: Int) -> Int

    func nextUniform() -> Float

    func nextBool() -> Bool
}
