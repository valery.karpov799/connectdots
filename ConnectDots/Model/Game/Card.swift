//
//  Card.swift
//  ConnectDots
//
//  Created by Mark on 04.04.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation

struct Card: Equatable, Hashable {
    let number: Int
}
