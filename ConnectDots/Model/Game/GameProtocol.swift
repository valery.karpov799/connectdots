//
//  GameProtocol.swift
//  ConnectDots
//
//  Created by Mark on 04.04.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation

protocol GameProtocol {
    var rows: Int { get }
    var columns: Int { get }
    func start()
    func touchBegan(at position: Position)
    func touchGoesThrough(_ position: Position)
    func touchEnd()
}
