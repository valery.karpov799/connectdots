//
//  Game.swift
//  Concentration
//
//  Created by Mark on 30.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation
import GameplayKit

protocol Random: GKRandom {}

class Game: GameProtocol {
    private(set) var rows: Int
    private(set) var columns: Int
    private(set) var score = 0
    private(set) var scoreNeeded: Int
    private(set) var steps: Int
    var randomGenerator: GKRandom!
    weak var scene: GameSceneProtocol!

    init(rows: Int, columns: Int, scoreNeeded: Int, steps: Int) {
        self.rows = rows
        self.columns = columns
        self.scoreNeeded = scoreNeeded
        self.steps = steps
        self.cards = Array(
            repeating: Array(
                repeating: nil,
                count: columns
            ),
            count: rows
        )
    }

    func start() {
        var additions = Set<BoardUpdate>()
        for row in 0..<rows {
            for column in 0..<columns {
                let card = Card(number: randomGenerator.nextInt())
                cards[row][column] = card
                additions.insert(.initialize(
                    card,
                    at: .init(
                        row: row,
                        column: column
                    )
                ))
            }
        }
        scene.display(sceneModel: makeModel(boardUpdates: additions))
    }

    func touchBegan(at position: Position) {
        guard card(at: position) != nil else { return }
        path = [position]
        scene.display(sceneModel: makeModel(boardUpdates: [.select(at: position)]))
    }

    func touchGoesThrough(_ position: Position) {
        guard !path.contains(position),
            let lastPosition = path.last,
            card(at: position)?.number == card(at: lastPosition)?.number,
            position.row == lastPosition.row ||
            position.column == lastPosition.column else {
            return
        }
        path += [position]
        scene.display(sceneModel: makeModel(boardUpdates: [.select(at: position)]))
    }

    func touchEnd() {
        if path.count > 1 {
            var updates = Set<BoardUpdate>()
            let columns = Set(path.map { $0.column })
            for position in path {
                cards[position.row][position.column] = nil
                updates.insert(.removeCard(position))
            }
            for row in stride(from: rows - 1, through: 0, by: -1) {
                for column in columns {
                    if row > 0, cards[row][column] == nil {
                        for i in stride(from: row - 1, through: 0, by: -1) {
                            if cards[i][column] == nil {
                                continue
                            } else {
                                cards[row][column] = cards[i][column]
                                cards[i][column] = nil
                                updates.insert(.move(
                                    from: .init(
                                        row: i,
                                        column: column
                                    ),
                                    to: .init(
                                        row: row,
                                        column: column
                                    )
                                ))
                                break
                            }
                        }
                    }
                }
            }
            for row in 0..<rows {
                for column in columns {
                    if cards[row][column] == nil {
                        let card = Card(number: randomGenerator.nextInt())
                        cards[row][column] = card
                        updates.insert(.add(
                            card,
                            at: .init(
                                row: row,
                                column: column
                            )
                        ))
                    }
                }
            }
            score += path.count
            steps -= 1
            if score >= scoreNeeded {
                status = .win
            } else if steps == 0 {
                status = .lose
            }
            scene.display(sceneModel: makeModel(boardUpdates: updates))
        } else {
            guard let lastPosition = path.last else { return }
            scene.display(sceneModel: makeModel(boardUpdates: [.deselect(at: lastPosition)]))
        }
    }

    private var cards = [[Card?]]()
    private var path = [Position]()
    private var status = SceneModel.Status.started

    private func card(at position: Position) -> Card? {
        return cards[position.row][position.column]
    }

    private func makeModel(boardUpdates: Set<BoardUpdate>) -> SceneModel {
        return .init(
            status: status,
            steps: steps,
            score: score,
            scoreNeeded: scoreNeeded,
            boardUpdates: boardUpdates
        )
    }
}

private extension Position {
    func lower(than position: Position) -> Bool {
        return row > position.row
    }
}
