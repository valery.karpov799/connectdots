//
//  GameSceneProtocol.swift
//  ConnectDots
//
//  Created by Mark on 04.04.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation

struct Position: Equatable, Hashable {
    let row: Int
    let column: Int
}

enum BoardUpdate: Equatable, Hashable {
    case initialize(Card, at: Position)
    case add(Card, at: Position)
    case move(from: Position, to: Position)
    case select(at: Position)
    case deselect(at: Position)
    case removeCard(Position)
}

struct SceneModel: Equatable {
    enum Status: Equatable {
        case started, win, lose
    }

    let status: Status
    let steps: Int
    let score: Int
    let scoreNeeded: Int
    let boardUpdates: Set<BoardUpdate>
}

protocol GameSceneProtocol: class {
    func display(sceneModel: SceneModel)
}
