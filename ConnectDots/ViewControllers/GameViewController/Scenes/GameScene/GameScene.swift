//
//  GameScene.swift
//  Concentration
//
//  Created by Mark on 23.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import SpriteKit

protocol GameSceneDelegate: class {
    func playerLost(gameScene: GameScene)
    func playerWin(gameScene: GameScene)
}

class GameScene: SKScene, GameSceneProtocol {
    var game: GameProtocol!
    weak var gameSceneDelegate: GameSceneDelegate?

    override func sceneDidLoad() {
        super.sceneDidLoad()
        backgroundColor = .clear
        physicsWorld.gravity = .init(dx: 0, dy: -10)
        addChild(pathNode)
        stepsLabel.fontName = UIFont.baseFontName
        stepsLabel.fontColor = .primary
        scoreLabel.fontName = UIFont.baseFontName
        scoreLabel.fontColor = .primary
        isUserInteractionEnabled = true
    }

    override func didMove(to view: SKView) {
        super.didMove(to: view)
        layoutNodes()
    }

    override func didChangeSize(_ oldSize: CGSize) {
        super.didChangeSize(oldSize)
        layoutNodes()
    }

    override func update(_ currentTime: TimeInterval) {
        super.update(currentTime)
        pathNode.path = cgPath
    }

    func display(sceneModel: SceneModel) {
        var actions = [SKAction]()
        stepsLabel.text = "STEPS \(sceneModel.steps)"
        scoreLabel.text = "\(score) / \(sceneModel.scoreNeeded)"
        if sceneModel.score != score {
            actions.append(.run { [unowned self] in
                self.scoreLabel.run(.customAction(withDuration: 0.25, actionBlock: { [unowned self] (label, t) in
                    self.scoreLabel.text = "\(self.score + Int(t / 0.25 * CGFloat(sceneModel.score - self.score))) / \(sceneModel.scoreNeeded)"
                })) {
                    self.score = sceneModel.score
                }
            })
        }
        let depth: Int! = maxDepth(for: sceneModel.boardUpdates)
        for update in sceneModel.boardUpdates {
            switch update {
            case let .initialize(card, at: position):
                let cardNode = CardNode(
                    size: .init(
                        width: max(0, cardWidth),
                        height: max(0, cardWidth)
                    ),
                    number: card.number,
                    boardPosition: position
                )
                cardNode.position = positionForCard(at: position)
                cardNode.alpha = 0
                addChild(cardNode)
                actions.append(.run {
                    cardNode.run(.sequence([
                        .wait(forDuration: .random(in: 0...0.5)),
                        .fadeIn(withDuration: 0.25)
                    ]))
                })
            case let .add(card, at: position):
                let cardNode = CardNode(
                    size: .init(
                        width: max(0, cardWidth),
                        height: max(0, cardWidth)
                    ),
                    number: card.number,
                    boardPosition: position
                )
                cardNode.position = positionForCard(at: .init(row: -depth + position.row - 1, column: position.column))
                addChild(cardNode)
                actions.append(.run { [unowned self] in
                    let fall = SKAction.move(to: self.positionForCard(at: position), duration: 0.25)
                    fall.timingFunction = SKActionTimingFunctions.easeOutElastic
                    cardNode.run(fall)
                })
            case let .select(at: position):
                guard let cardNode = children.compactMap({ $0 as? CardNode })
                    .first(where: { $0.boardPosition == position }) else {
                    return
                }
                cardNode.select()
                path.append(cardNode.position)
            case let .deselect(at: position):
                children.compactMap { $0 as? CardNode }
                    .first { $0.boardPosition == position }?
                    .deselect()
            case let .move(from: fromPosition, to: toPosition):
                guard let cardNode = children.compactMap({ $0 as? CardNode })
                    .first(where: { $0.boardPosition == fromPosition }) else {
                    return
                }
                actions.append(.run { [unowned self] in
                    let move = SKAction.move(to: self.positionForCard(at: toPosition), duration: 0.25)
                    move.timingFunction = SKActionTimingFunctions.easeOutElastic
                    cardNode.run(move) {
                        cardNode.boardPosition = toPosition
                    }
                })
            case let .removeCard(position):
                guard let cardNode = children.compactMap({ $0 as? CardNode })
                    .first(where: { $0.boardPosition == position }) else {
                    return
                }
                cardNode.zPosition = -2
                cardNode.remove()
                actions.append(.run {
                    cardNode.run(.fadeAlpha(to: 0, duration: 0.25)) {
                        cardNode.removeFromParent()
                    }
                })
            }
        }
        switch sceneModel.status {
        case .win:
            actions.append(.sequence([
                .wait(forDuration: 0.25),
                .group(makeWinActions()),
                .wait(forDuration: 2),
                .run { self.gameSceneDelegate?.playerWin(gameScene: self) }
            ]))
        case .lose:
            actions.append(.sequence([
                .wait(forDuration: 0.25),
                .group(makeLoseActions()),
                .wait(forDuration: 1),
                .run { self.gameSceneDelegate?.playerLost(gameScene: self) }
            ]))
        default:
            break
        }
        guard !actions.isEmpty else { return }
        run(.sequence([
            .run { [unowned self] in
                self.isUserInteractionEnabled = false
            },
            .group(actions),
            .run { [unowned self] in
                self.isUserInteractionEnabled = true
            }
        ]))
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let location = touch.location(in: self)
        guard let cardNode = nodes(at: location).compactMap({ $0 as? CardNode }).first else {
            return
        }
        touchLocation = location
        game.touchBegan(at: cardNode.boardPosition)
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let location = touch.location(in: self)
        touchLocation = location
        guard let cardNode = nodes(at: location).compactMap({ $0 as? CardNode }).first else {
            return
        }
        game.touchGoesThrough(cardNode.boardPosition)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchLocation = nil
        path = []
        game.touchEnd()
    }

    private let margin: CGFloat = (Bundle.main.infoDictionary?["CardsMargin"] as? CGFloat) ?? 0

    private var layoutMargins: UIEdgeInsets {
        return view?.layoutMargins ?? .zero
    }

    private var scoreLabel: SKLabelNode! {
        return self["//scoreLabel"].first as? SKLabelNode
    }

    private var scoreLabelNode: SKNode! {
        return self["//scoreLabelNode"].first
    }

    private var stepsLabelNode: SKNode! {
        return self["//stepsLabelNode"].first
    }

    private var stepsLabel: SKLabelNode! {
        return self["//stepsLabel"].first as? SKLabelNode
    }

    private lazy var pathNode: SKShapeNode = {
        let pathNode = SKShapeNode()
        pathNode.zPosition = -1
        pathNode.lineWidth = 2
        pathNode.glowWidth = 4
        pathNode.lineCap = .round
        pathNode.lineJoin = .round
        pathNode.strokeColor = .primary
        return pathNode
    }()

    private var cgPath: CGPath! {
        guard let touchLocation = touchLocation,
            !path.isEmpty else {
            return nil
        }
        let cgPath = CGMutablePath()
        cgPath.addLines(between: path)
        cgPath.addLine(to: touchLocation)
        return cgPath
    }

    private var touchLocation: CGPoint?
    private var path: [CGPoint] = []
    private var score: Int = 0

    private var fieldWidth: CGFloat {
        return size.width - layoutMargins.left - layoutMargins.right
    }

    private var fieldHeight: CGFloat {
        return size.height
            - layoutMargins.top
            - layoutMargins.bottom
            - max(scoreLabelNode.frame.height, stepsLabelNode.frame.height)
            - 16
    }

    private var horizontalCenteringOffset: CGFloat {
        let cardsWidth = CGFloat(game.columns) * cardWidth + CGFloat(game.columns - 1) * margin
        return (fieldWidth - cardsWidth) / 2.0
    }

    private var cardWidth: CGFloat {
        let minimalSide = min(fieldWidth, fieldHeight)
        let minimalDimension = min(game.rows, game.columns)
        return (minimalSide - margin * CGFloat(minimalDimension - 1)) / CGFloat(minimalDimension)
    }

    private func layoutNodes() {
        stepsLabelNode.position = CGPoint(
            x: size.width / 2 - stepsLabelNode.frame.size.width / 2 - 16,
            y: size.height - stepsLabelNode.frame.size.height / 2 - layoutMargins.top
        )
        scoreLabelNode.position = CGPoint(
            x: size.width / 2 + scoreLabelNode.frame.size.width / 2 + 16,
            y: size.height - scoreLabelNode.frame.size.height / 2 - layoutMargins.top
        )
        for cardNode in children.compactMap({ $0 as? CardNode }) {
            cardNode.position = positionForCard(at: cardNode.boardPosition)
            cardNode.size = .init(width: cardWidth, height: cardWidth)
        }
    }

    private func positionForCard(at position: Position) -> CGPoint {
        return CGPoint(
            x: CGFloat(position.column) * (cardWidth + margin) + cardWidth / 2.0 + layoutMargins.left + horizontalCenteringOffset,
            y: CGFloat(game.rows - position.row - 1) * (cardWidth + margin) + layoutMargins.bottom + cardWidth / 2.0
        )
    }

    private func maxDepth(for updates: Set<BoardUpdate>) -> Int? {
        return updates.compactMap { update in
            guard case let .add(_, position) = update else { return nil }
            return position.row
        }.max()
    }

    private func makeLoseActions() -> [SKAction] {
        var actions = [SKAction]()
        let cards = children.compactMap { $0 as? CardNode }
        for card in cards {
            actions.append(.run {
                card.physicsBody = SKPhysicsBody(rectangleOf: card.size)
                card.physicsBody?.mass = 1
            })
        }
        actions.append(.run { [unowned self] in
            self.scoreLabelNode.run(.fadeOut(withDuration: 0.5))
            self.stepsLabelNode.run(.fadeOut(withDuration: 0.5))
        })
        return actions
    }

    private func makeWinActions() -> [SKAction] {
        var actions = [SKAction]()
        let cards = children.compactMap { $0 as? CardNode }
        for card in cards {
            actions.append(.run { [unowned self] in
                card.physicsBody = SKPhysicsBody(rectangleOf: card.size)
                card.physicsBody?.mass = 1
                card.physicsBody?.applyImpulse(CGVector(
                    dx: .random(in: -self.size.width...self.size.width),
                    dy: 1000
                ))
                let zDistance = CGFloat.random(in: 0.5...0.9)
                card.run(.scale(by: zDistance, duration: 1))
                card.spriteNode.run(.colorize(
                    with: UIColor.black,
                    colorBlendFactor: zDistance,
                    duration: 1
                ))
            })
        }
        return actions
    }
}
