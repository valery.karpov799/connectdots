//
//  SKTimingFunction.swift
//  Studious ToolKit
//
//  Created by Allan Weir on 09/01/2017.
//  Copyright © 2017 Allan Weir. All rights reserved.
//  Adapted from a JavaScript version found at https://gist.github.com/gre/1650294
//

import SpriteKit

enum SKActionTimingFunctions {
    static let easeOutElastic: SKActionTimingFunction = { t in
        if t < 1.0 / 2.75 {
           return 7.5625 * t * t * t
         } else if t < 2.0 / 2.75 {
           let f = t - 1.5 / 2.75
           return 7.5625 * f * f + 0.75
         } else if t < 2.5 / 2.75 {
           let f = t - 2.25 / 2.75
           return 7.5625 * f * f + 0.9375
         } else {
           let f = t - 2.625 / 2.75
           return 7.5625 * f * f + 0.984375
         }
    }
}
