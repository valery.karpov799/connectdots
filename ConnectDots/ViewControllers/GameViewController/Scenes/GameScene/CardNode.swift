//
//  CardNode.swift
//  Concentration
//
//  Created by Mark on 30.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation
import SpriteKit

class CardNode: SKNode {
    let number: Int
    var boardPosition: Position

    var size: CGSize {
        didSet {
            removingEmitterNode.particlePositionRange = .init(
                dx: size.width / 2,
                dy: size.height / 2
            )
            spriteNode.size = size
            bluredSpriteNode.size = size
        }
    }

    init(size: CGSize = .zero, number: Int, boardPosition: Position) {
        self.number = number
        self.boardPosition = boardPosition
        self.size = size
        super.init()
        addChild(effectNode)
        addChild(spriteNode)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func select() {
        let fadeIn = SKAction.fadeIn(withDuration: 0.25)
        fadeIn.timingMode = .easeIn
        effectNode.run(fadeIn)
    }

    func deselect() {
        let fadeOut = SKAction.fadeOut(withDuration: 0.25)
        fadeOut.timingMode = .easeOut
        effectNode.run(fadeOut)
    }

    func remove() {
        addChild(removingEmitterNode)
    }

    private lazy var bluredSpriteNode: SKSpriteNode = {
        let color: UIColor! = .primary
        var image: UIImage! = UIImage(named: "card\(number)")?
            .withRenderingMode(.alwaysTemplate)
        let bluredSpriteNode = SKSpriteNode(
            texture: SKTexture(image: image),
            color: color,
            size: size
        )
        bluredSpriteNode.colorBlendFactor = 1
        return bluredSpriteNode
    }()

    private(set) lazy var spriteNode: SKSpriteNode = .init(
        texture: SKTexture(imageNamed: "card\(number)"),
        color: .clear,
        size: size
    )

    private lazy var removingEmitterNode: SKEmitterNode! = {
        let removingEmitterNode = SKEmitterNode(fileNamed: "Selected")
        removingEmitterNode?.position = .zero
        removingEmitterNode?.targetNode = self
        
        removingEmitterNode?.particleColor = .primary
        return removingEmitterNode
    }()

    private lazy var effectNode: SKEffectNode = {
        let effectNode = SKEffectNode()
        effectNode.shouldRasterize = true
        effectNode.addChild(bluredSpriteNode)
        effectNode.filter = CIFilter(name: "CIGaussianBlur", parameters: ["inputRadius": 8])
        effectNode.alpha = 0
        return effectNode
    }()
}
