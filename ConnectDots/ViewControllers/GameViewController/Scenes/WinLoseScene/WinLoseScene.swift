//
//  WinLoseScene.swift
//  Concentration
//
//  Created by Mark on 29.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation
import SpriteKit

protocol WinLoseSceneDelegate: class {
    func onContinuePressed(_ winScene: WinLoseScene)
}

class WinLoseScene: SKScene {
    enum Mode {
        case win, lose
    }

    var mode: Mode = .win {
        didSet {
            switch mode {
            case .lose:
                imageNode.texture = SKTexture(imageNamed: "game-over")
                buttonNode.labelNode.text = "TRY AGAIN"
            case .win:
                imageNode.texture = SKTexture(imageNamed: "you-win")
                buttonNode.labelNode.text = "CONTINUE"
            }
        }
    }

    var showsContinueButton: Bool = true {
        didSet {
            buttonNode.isHidden = !showsContinueButton
            if !showsContinueButton {
                continueTimer?.invalidate()
                continueTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { [weak self] _ in
                    guard let self = self else { return }
                    self.winLoseSceneDelegate?.onContinuePressed(self)
                }
            }
        }
    }

    weak var winLoseSceneDelegate: WinLoseSceneDelegate?
    let padding: CGFloat = 16

    deinit {
        continueTimer?.invalidate()
    }

    override func sceneDidLoad() {
        super.sceneDidLoad()
        buttonNode.onTap = { [unowned self] in
            self.winLoseSceneDelegate?.onContinuePressed(self)
        }
        isSceneLoaded = true
    }

    override func didMove(to view: SKView) {
        super.didMove(to: view)
        let imageWidth = min(size.width, size.height) - 2 * padding
        let imageSize = CGSize(width: imageWidth, height: imageNode.size.height / imageNode.size.width * imageWidth)
        imageNode.run(.sequence([
            .scale(to: .zero, duration: 0),
            .scale(to: imageSize, duration: 0.5)
        ]))
        buttonNode.run(.sequence([
            .moveTo(y: -size.height - buttonNode.size.height, duration: 0),
            .move(to: buttonNode.position, duration: 0.5)
        ]))
    }

    override func didChangeSize(_ oldSize: CGSize) {
        super.didChangeSize(oldSize)
        guard isSceneLoaded else { return }
        let imageAspectRatio = imageNode.size.height / imageNode.size.width
        let imageWidth = size.width - padding * 2
        imageNode.size = CGSize(width: imageWidth, height: imageWidth * imageAspectRatio)
        let buttonAspectRatio = buttonNode.size.height / imageNode.size.width
        buttonNode.size = CGSize(width: imageWidth, height: imageWidth * buttonAspectRatio)
        let bottomInset = UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0
        let buttonPositionY = (-size.height + buttonNode.size.height) / 2
            + bottomInset
            + padding
        buttonNode.position = CGPoint(x: 0, y: buttonPositionY)
    }

    private var continueTimer: Timer!

    private var imageNode: SKSpriteNode! {
        return self["//imageNode"].first as? SKSpriteNode
    }

    private var buttonNode: ButtonNode! {
        return self["//buttonNode"].first as? ButtonNode
    }

    private var isSceneLoaded = false
}
