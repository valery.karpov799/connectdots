//
//  GameViewController.swift
//  Concentration
//
//  Created by Mark on 23.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return Bundle.main.rows >= Bundle.main.columns
            ? .portrait
            : .landscapeRight
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.frame = view.bounds
        backgroundView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(backgroundView)
        sceneView.frame = view.bounds
        sceneView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(sceneView)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard sceneView.scene == nil else { return }
        sceneView.presentScene(gameScene())
    }

    private lazy var backgroundView: UIView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "background")
        view.contentMode = .scaleAspectFill
        return view
    }()

    private lazy var sceneView: SKView = {
        let sceneView = SKView()
        sceneView.backgroundColor = .clear
        sceneView.layoutMargins = supportedInterfaceOrientations == .portrait
            ? .init(top: 16, left: 16, bottom: 16, right: 16)
            : .init(top: 16, left: 72, bottom: 16, right: 72)
        sceneView.insetsLayoutMarginsFromSafeArea = true
        return sceneView
    }()

    private var level: Int {
        get { return UserDefaults.standard.integer(forKey: "level") }
        set { UserDefaults.standard.set(newValue, forKey: "level")}
    }

    private var scoreNeeded: Int {
        return .scoreNeededStart + .scoreNeededStep * level
    }

    private var steps: Int {
        return Bundle.main.stepsStart + .stepsStep * level
    }

    private func gameScene() -> GameScene! {
        let game = Game(
            rows: Bundle.main.rows,
            columns: Bundle.main.columns,
            scoreNeeded: scoreNeeded,
            steps: steps
        )
        game.randomGenerator = GKRandomDistribution(
            randomSource: GKRandomSource(),
            lowestValue: 0,
            highestValue: Bundle.main.cardsCount - 1
        )
        guard let gameScene = GKScene(fileNamed: "GameScene")?.rootNode as? GameScene else {
            return nil
        }
        gameScene.anchorPoint = CGPoint(x: 0, y: 0)
        gameScene.game = game
        gameScene.scaleMode = .resizeFill
        gameScene.gameSceneDelegate = self
        game.scene = gameScene
        game.start()
        return gameScene
    }
}

extension GameViewController: GameSceneDelegate {
    func playerLost(gameScene: GameScene) {
        let loseScene: WinLoseScene! = GKScene(fileNamed: "WinLoseScene")?.rootNode as? WinLoseScene
        loseScene.mode = .lose
        loseScene.winLoseSceneDelegate = self
        loseScene.showsContinueButton = supportedInterfaceOrientations == .portrait
        sceneView.presentScene(loseScene)
    }

    func playerWin(gameScene: GameScene) {
        level += 1
        let winScene: WinLoseScene! = GKScene(fileNamed: "WinLoseScene")?.rootNode as? WinLoseScene
        winScene.mode = .win
        winScene.showsContinueButton = supportedInterfaceOrientations == .portrait
        winScene.winLoseSceneDelegate = self
        sceneView.presentScene(winScene)
    }
}

extension GameViewController: WinLoseSceneDelegate {
    func onContinuePressed(_ winScene: WinLoseScene) {
        sceneView.presentScene(gameScene())
    }
}

private extension Int {
    static let stepsStep: Int! = Bundle.main.infoDictionary?["GameStepsStep"] as? Int
    static let scoreNeededStart: Int! = Bundle.main.infoDictionary?["GameScoreNeededStart"] as? Int
    static let scoreNeededStep: Int! = Bundle.main.infoDictionary?["GameScoreNeededStep"] as? Int
}
