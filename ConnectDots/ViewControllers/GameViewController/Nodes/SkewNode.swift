//
//  SkewNode.swift
//  Concentration
//
//  Created by Mark on 28.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation
import SpriteKit

class SkewNode: SKEffectNode {
    override init() {
        super.init()
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        let affineTransform = CGAffineTransform(a: 1, b: 0.1, c: 0, d: 1, tx: 0, ty: 0)
        let transformFilter = CIFilter(name: "CIAffineTransform")!
        transformFilter.setValue(NSValue(cgAffineTransform: affineTransform), forKey: "inputTransform")
        filter = transformFilter
        shouldRasterize = true
        shouldEnableEffects = true
    }
}
