//
//  ButtonNode.swift
//  Concentration
//
//  Created by Mark on 29.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation
import SpriteKit

class ButtonNode: SKSpriteNode {
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    var labelNode: SKLabelNode! {
        return self["//labelNode"].first as? SKLabelNode
    }

    var onTap: (() -> Void)?

    private func setup() {
        labelNode?.fontColor = .primary
        labelNode?.fontSize = 24
        labelNode?.blendMode = .multiplyX2
        labelNode?.fontName = UIFont.baseFontName
        isUserInteractionEnabled = true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        run(.fadeAlpha(to: 0.8, duration: 0.1))
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        run(.fadeAlpha(to: 1, duration: 0.1))
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        run(.fadeAlpha(to: 1, duration: 0.1))
        guard let touch = touches.first, touch.tapCount > 0
        else { return }
        onTap?()
    }
}
