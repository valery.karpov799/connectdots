//
//  UIFont+Extensions.swift
//  Concentration
//
//  Created by Mark on 29.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    static let baseFontName: String! = Bundle.main.infoDictionary?["BaseFontName"] as? String
}
