//
//  AppDelegate.swift
//  Concentration
//
//  Created by Mark on 23.01.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import UIKit
import AmoTools

@UIApplicationMain
class AppDelegate: AmoTools.AppDelegate {
    override func makeMainViewController() -> UIViewController {
        return GameViewController()
    }

    override func application(_ application: UIApplication,
                              supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .allButUpsideDown
    }
}
