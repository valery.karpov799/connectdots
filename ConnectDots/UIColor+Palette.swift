//
//  UIColor+Palette.swift
//  ConnectDots
//
//  Created by Mark on 05.04.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let primary: UIColor! = UIColor(named: "primary")
    static let accent: UIColor! = UIColor(named: "accent")
}
