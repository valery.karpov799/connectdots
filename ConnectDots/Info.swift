//
//  Info.swift
//  ConnectDots
//
//  Created by Mark on 20.04.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import Foundation
import UIKit

extension Bundle {
    var rows: Int {
        guard let rows = infoDictionary?["GameRows"] as? String else { return 8 }
        return Int(rows) ?? 8
    }

    var columns: Int {
        guard let columns = infoDictionary?["GameColumns"] as? String else { return 5 }
        return Int(columns) ?? 5
    }

    var stepsStart: Int {
        guard let columns = infoDictionary?["GameStepsStart"] as? String else { return 10 }
        return Int(columns) ?? 10
    }

    var cardsCount: Int {
        guard let cardsCount = infoDictionary?["GameCardsCount"] as? String else { return 10 }
        return Int(cardsCount) ?? 10
    }
}
