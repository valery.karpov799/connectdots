//
//  GameTests.swift
//  ConnectDotsTests
//
//  Created by Mark on 03.04.2020.
//  Copyright © 2020 Grig Teriz. All rights reserved.
//

import XCTest
import GameplayKit
import Mockingbird
@testable import ConnectDots

class GameTests: XCTestCase {
    var scene: GameSceneProtocolMock!
    var subject: Game!
    var random: RandomMock!

    override func setUp() {
        scene = GameSceneProtocolMock()
        random = mock(Random.self)
        makeSubject()
        given(random.nextInt()) ~> 0
    }

    func testStart_itAddsCardsToTheBoard() {
        subject.start()

        verify(scene.display(sceneModel: SceneModel(
            status: .started,
            steps: 1,
            score: 0,
            scoreNeeded: 4,
            boardUpdates: [
                .initialize(Card(number: 0), at: .init(row: 0, column: 0)),
                .initialize(Card(number: 0), at: .init(row: 0, column: 1)),
                .initialize(Card(number: 0), at: .init(row: 1, column: 0)),
                .initialize(Card(number: 0), at: .init(row: 1, column: 1))
            ]
        ))).wasCalled()
    }

    func testTouchBegan_itSelectsAppropriateCard() {
        subject.start()

        subject.touchBegan(at: .init(row: 0, column: 0))

        let argCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argCaptor.matcher)).wasCalled(exactly(2))
        XCTAssertEqual(
            argCaptor.value?.boardUpdates,
            [.select(at: .init(row: 0, column: 0))]
        )
    }

    func testTouchGoesThrough_whenCardsHaveEqualNumbers_itSelectsNextCard() {
        subject.start()
        subject.touchBegan(at: .init(row: 0, column: 0))

        subject.touchGoesThrough(.init(row: 0, column: 1))

        let argCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argCaptor.matcher)).wasCalled(atLeast(1))
        XCTAssertEqual(
            argCaptor.allValues.last?.boardUpdates,
            [.select(at: .init(row: 0, column: 1))]
        )
    }

    func testTouchGoesThrough_whenCardsHaveUnequalNumbers_itDoesntSelectNextCard() {
        var numbers = [1, 0]
        given(random.nextInt()) ~> (numbers.popLast() ?? 0)
        subject.start()
        subject.touchBegan(at: .init(row: 0, column: 0))

        subject.touchGoesThrough(.init(row: 0, column: 1))

        verify(scene.display(sceneModel: any())).wasCalled(exactly(2))
    }

    func testTouchGoesThrough_whenCardsHaveNoEqualDimension_itDoesntSelectNextCard() {
        subject.start()
        subject.touchBegan(at: .init(row: 0, column: 0))

        subject.touchGoesThrough(.init(row: 1, column: 1))

        verify(scene.display(sceneModel: any())).wasCalled(exactly(2))
    }

    func testTouchesGoesThrough_whenPathContainsNextCard_itDoesntSelectNextCard() {
        subject.start()
        subject.touchBegan(at: .init(row: 0, column: 0))
        subject.touchGoesThrough(.init(row: 0, column: 1))

        subject.touchGoesThrough(.init(row: 0, column: 0))

        verify(scene.display(sceneModel: any())).wasCalled(exactly(3))
    }

    func testTouchEnd_whenPathContainsOneCard_itDeselectsIt() {
        subject.start()
        subject.touchBegan(at: .init(row: 0, column: 0))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(3))
        XCTAssertEqual(
            argumentCaptor.value?.boardUpdates,
            [.deselect(at: .init(row: 0, column: 0))]
        )
    }

    func testTouchEnd_whenPathContainsMoreThanOneCard_itRemovesIt() {
        subject.start()
        subject.touchBegan(at: .init(row: 0, column: 0))
        subject.touchGoesThrough(.init(row: 0, column: 1))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(4))
        let boardUpdates = argumentCaptor.value?.boardUpdates
        XCTAssertTrue(boardUpdates?.contains(.removeCard(.init(row: 0, column: 0))) ?? false)
        XCTAssertTrue(boardUpdates?.contains(.removeCard(.init(row: 0, column: 0))) ?? false)
    }

    func testTouchEnd_whenPathContainsMoreThanOneCard_itAddsPathLengthToScore() {
        subject.start()
        subject.touchBegan(at: .init(row: 0, column: 0))
        subject.touchGoesThrough(.init(row: 0, column: 1))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(4))
        XCTAssertEqual(argumentCaptor.value?.score, 2)
    }

    func testTouchEnd_whenPathContainsMoreThanOneCard_whenNotFirstRow_itMovesUpperCardsToRemovedPositions() {
        makeSubject(rows: 3, columns: 2)
        subject.start()
        subject.touchBegan(at: .init(row: 1, column: 0))
        subject.touchGoesThrough(.init(row: 1, column: 1))
        subject.touchGoesThrough(.init(row: 2, column: 1))
        subject.touchGoesThrough(.init(row: 2, column: 0))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(6))
        let boardUpdates = argumentCaptor.value?.boardUpdates
        XCTAssertTrue(
            boardUpdates?.contains(.move(
                from: .init(row: 0, column: 0),
                to: .init(row: 2, column: 0)
            )) ?? false
        )
        XCTAssertTrue(
            boardUpdates?.contains(.move(
                from: .init(row: 0, column: 1),
                to: .init(row: 2, column: 1)
            )) ?? false
        )
    }

    func testTouchEnd_whenPathContainsMoreThanOneCard_itAddsNewCardsOnEmptyPositions() {
        subject.start()
        subject.touchBegan(at: .init(row: 1, column: 0))
        subject.touchGoesThrough(.init(row: 1, column: 1))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(4))
        let boardUpdates = argumentCaptor.value?.boardUpdates
        XCTAssertTrue(
            boardUpdates?.contains(.add(
                .init(number: 0),
                at: .init(
                    row: 0,
                    column: 0
                )
            )) ?? false
        )
        XCTAssertTrue(
            boardUpdates?.contains(.add(
                .init(number: 0),
                at: .init(
                    row: 0,
                    column: 1
                )
            )) ?? false
        )
    }

    func testTouchEnd_itDecreasesSteps() {
        subject.start()
        subject.touchBegan(at: .init(row: 1, column: 0))
        subject.touchGoesThrough(.init(row: 1, column: 1))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(4))
        XCTAssertEqual(argumentCaptor.value?.steps, 0)
    }

    func testTouchEnd_whenScoreGreaterThanOrEqualToScoreNeeded_itChangesStatusToWin() {
        subject.start()
        subject.touchBegan(at: .init(row: 1, column: 0))
        subject.touchGoesThrough(.init(row: 1, column: 1))
        subject.touchGoesThrough(.init(row: 0, column: 1))
        subject.touchGoesThrough(.init(row: 0, column: 0))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(6))
        XCTAssertEqual(argumentCaptor.value?.status, .win)
    }

    func testTouchEnd_whenGameInProgress_itDoesntChangeStatus() {
        makeSubject(steps: 2)
        subject.start()
        subject.touchBegan(at: .init(row: 1, column: 0))
        subject.touchGoesThrough(.init(row: 1, column: 1))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(4))
        XCTAssertEqual(argumentCaptor.value?.status, .started)
    }

    func testTouchEnd_whenStepsEqual0_itChangesStatusToLose() {
        subject.start()
        subject.touchBegan(at: .init(row: 1, column: 0))
        subject.touchGoesThrough(.init(row: 1, column: 1))

        subject.touchEnd()

        let argumentCaptor = ArgumentCaptor<SceneModel>()
        verify(scene.display(sceneModel: argumentCaptor.matcher)).wasCalled(exactly(4))
        XCTAssertEqual(argumentCaptor.value?.status, .lose)
    }

    private func makeSubject(rows: Int = 2,
                             columns: Int = 2,
                             scoreNeeded: Int = 4,
                             steps: Int = 1) {
        subject = Game(
            rows: rows,
            columns: columns,
            scoreNeeded: scoreNeeded,
            steps: steps
        )
        subject.scene = scene
        subject.randomGenerator = random
    }
}
